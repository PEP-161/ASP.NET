﻿using Visitors.Domain.Abstract;
using Visitors.Domain.Enums;

namespace Visitors.Domain
{
    public abstract class Human : Entity
    {
        public string FirstName { get; set; }       
        public string LastName { get; set; }       
        public string Phone { get; set; }
        public Gender Gender { get; set; }       
        public string IIN { get; set; }
    }
}
