﻿using System.ComponentModel.DataAnnotations;

namespace Visitors.Domain.Enums
{
    public enum Gender : byte
    {
        [Display(Name = "Не выбран")]
        NotSet = 0,
        [Display(Name = "Женский")]
        Female = 1,
        [Display(Name = "Мужской")]
        Male = 2
    }
}
