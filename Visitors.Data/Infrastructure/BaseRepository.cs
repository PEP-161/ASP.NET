﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Visitors.Domain.Abstract;

namespace Visitors.Data.Infrastructure
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class, IEntity
    {
        private readonly DataContext _context;
        private readonly DbSet<T> _set;

        public BaseRepository(DataContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }
        public async Task<IQueryable<T>> GetAllAsync(Expression<Func<T, bool>> predicate)
        {
            return await Task.FromResult(_set.Where(predicate));
        }

        public async Task<IQueryable<T>> GetAllAsync(params Expression<Func<T, object>>[] includProperties)
        {
            IQueryable<T> query = _set;
            foreach (var prop in includProperties)
            {
                query = query.Include(prop);
            }
            return await Task.FromResult(query);
        }

        public async Task<T> GetAsync(Guid id)
        {
            return await _set.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> GetAsync(Guid id, params Expression<Func<T, object>>[] includProperties)
        {
            IQueryable<T> query = _set;
            foreach (var prop in includProperties)
            {
                query = query.Include(prop);
            }
            return await query.FirstOrDefaultAsync(x => x.Id == id);
        }


        public async Task<T> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await _set.FirstOrDefaultAsync(predicate);
        }

        public async Task AddAsync(T entity)
        {
            await _set.AddAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await Task.FromResult(_context.Entry(entity).State = EntityState.Modified);
        }

        public async Task DeleteAsync(T entity)
        {
            //_context.Entry(entity).State = EntityState.Deleted;
            entity.State = Domain.Enums.EntityState.Deleted;
            await UpdateAsync(entity);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
