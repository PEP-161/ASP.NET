﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Visitors.Domain.Abstract;

namespace Visitors.Data.Infrastructure
{
    public interface IBaseRepository<T> where T : IEntity
    {
        Task<IQueryable<T>> GetAllAsync(Expression<Func<T, bool>> predicate);
        Task<IQueryable<T>> GetAllAsync(params Expression<Func<T, object>>[] includProperties);
        Task<T> GetAsync(Guid id);
        Task<T> GetAsync(Guid id, params Expression<Func<T, object>>[] includProperties);
        Task<T> GetAsync(Expression<Func<T, bool>> predicate);
        Task AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task SaveChangesAsync();
    }
}
