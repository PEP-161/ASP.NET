﻿using Microsoft.EntityFrameworkCore;
using Visitors.Domain;

namespace Visitors.Data.Extensions
{
    public static class DataContextExtensions
    {
        public static void ConfigureVisitorModel(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Visitor>().HasKey(x => x.Id);
            modelBuilder.Entity<Visitor>().Property(x => x.FirstName).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Visitor>().Property(x => x.LastName).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Visitor>().Property(x => x.Phone).IsRequired().HasMaxLength(20);
            modelBuilder.Entity<Visitor>().Property(x => x.IIN).IsRequired().HasMaxLength(12);
            modelBuilder.Entity<Visitor>().Property(x => x.Address).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Visitor>().Property(x => x.Gender).IsRequired();
            modelBuilder.Entity<Visitor>()
                .HasMany(x => x.Visits)
                .WithOne(x => x.VisitorPerson)
                .HasForeignKey(x => x.VisitorPersonId);
        }
        public static void ConfigureVisitedModel(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().HasKey(x => x.Id);
            modelBuilder.Entity<Person>().Property(x => x.FirstName).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Person>().Property(x => x.LastName).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Person>().Property(x => x.Phone).IsRequired().HasMaxLength(20);
            modelBuilder.Entity<Person>().Property(x => x.IIN).IsRequired().HasMaxLength(12);
            modelBuilder.Entity<Person>().Property(x => x.Gender).IsRequired();
            modelBuilder.Entity<Person>()
                .HasMany(x => x.Visits)
                .WithOne(x => x.VisitedPerson)
                .HasForeignKey(x => x.VisitedPersonId);
        }
        public static void ConfigureVisitModel(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Visit>().HasKey(x => x.Id);
            modelBuilder.Entity<Visit>().Property(x => x.Floor).IsRequired();
            modelBuilder.Entity<Visit>().Property(x => x.Room).IsRequired().HasMaxLength(10);
            modelBuilder.Entity<Visit>().Property(x => x.VisitTime).IsRequired();
            modelBuilder.Entity<Visit>()
                .HasOne(x => x.VisitorPerson)
                .WithMany(x => x.Visits)
                .HasForeignKey(x => x.VisitorPersonId);
            modelBuilder.Entity<Visit>()
                .HasOne(x => x.VisitedPerson)
                .WithMany(x => x.Visits)
                .HasForeignKey(x => x.VisitedPersonId);
        }

    }
}
