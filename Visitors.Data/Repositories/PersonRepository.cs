﻿using Visitors.Data.Infrastructure;
using Visitors.Domain;

namespace Visitors.Data.Repositories
{
    public class PersonRepository : BaseRepository<Person>, IPersonRepository
    {
        public PersonRepository(DataContext context) : base(context)
        {

        }
    }
}
