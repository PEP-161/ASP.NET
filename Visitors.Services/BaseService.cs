﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Visitors.Data.Infrastructure;
using Visitors.Domain.Abstract;
using Visitors.Domain.Enums;

namespace Visitors.Services
{
    public class BaseService<T> : IBaseService<T> where T : class, IEntity
    {
        protected readonly IBaseRepository<T> Repository;
        public BaseService(IBaseRepository<T> repository)
        {
            Repository = repository;
        }
        public async Task  AddAsync(T entity)
        {
           await Repository.AddAsync(entity);
        }

        public async Task<IQueryable<TU>> GetActiveAsync<TU>()
        {
             return await Task.FromResult((await Repository
                .GetAllAsync(x => x.State == EntityState.Active))
                .ProjectTo<TU>());
        }

        public async Task SaveAsync()
        {
            await Repository.SaveChangesAsync();
        }
    }
}
