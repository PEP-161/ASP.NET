﻿using Visitors.Data.Repositories;
using Visitors.Domain;

namespace Visitors.Services
{
    public class PeopleService : SuperBaseService<Person>
    {
        public PeopleService(IPersonRepository repository) : base(repository)
        {
        }
    }
}
