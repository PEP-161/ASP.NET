﻿using Visitors.Data.Repositories;
using Visitors.Domain;

namespace Visitors.Services
{
    public class VisitorsService : SuperBaseService<Visitor>
    {
        public VisitorsService(IVisitorRepository repository) : base(repository)
        {
        }
    }
}