﻿using System.Linq;
using System.Threading.Tasks;

namespace Visitors.Services
{
    public interface IBaseService<in T> where T : class
    {
        Task<IQueryable<TU>> GetActiveAsync<TU>();
        Task AddAsync(T entity);
        Task SaveAsync();
    }
}
