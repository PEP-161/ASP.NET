﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Visitors.Domain.Abstract;
using Visitors.Models;

namespace Visitors.Services
{
    public interface ISuperBaseService<T> : IBaseService<T> where T : class, IEntity
    {
        Task<T> GetAsync(Guid id);
        Task<T> GetAsync(Guid id, params Expression<Func<T, object>>[] includProperties);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task<T> GetByIinAsync(string iin);
        Task<IQueryable<DropDownRecord>> GetDropDownRecordsAsync();
    }
}
