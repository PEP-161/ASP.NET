﻿using Visitors.Data.Repositories;
using Visitors.Domain;

namespace Visitors.Services
{
    public class VisitsService : BaseService<Visit>
    {
        public VisitsService(IVisitRepository repository) : base(repository)
        {

        }
    }
}

