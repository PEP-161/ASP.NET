﻿using Microsoft.Extensions.DependencyInjection;
using Visitors.Data.Repositories;
using Visitors.Services;
using Visitors.Web.Services;

namespace Visitors.Web.Configuration
{
    public static class ServicesConfiguration
    {
        public static void RegisterTypes(this IServiceCollection services)
        {
            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            services.AddTransient<IVisitorRepository, VisitorRepository>();
            services.AddTransient<IPersonRepository, PersonRepository>();
            services.AddTransient<IVisitRepository, VisitRepository>();
            services.AddTransient<VisitorsService>();
            services.AddTransient<PeopleService>();
            services.AddTransient<VisitsService>();
            services.AddTransient(typeof(IBaseService<>), typeof(BaseService<>));
            services.AddTransient(typeof(ISuperBaseService<>), typeof(SuperBaseService<>));
        }
    }
}
