using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Visitors.Domain;
using Visitors.Domain.Enums;
using Visitors.Models.VisitorsViewModels;
using Visitors.Services;
using X.PagedList;

namespace Visitors.Web.Controllers
{
    public class VisitorsController : Controller
    {
        private readonly VisitorsService _visitorsService;

        public VisitorsController(VisitorsService visitorsService)
        {
            _visitorsService = visitorsService;
        }

        // GET: Visitors
        public async Task<IActionResult> Index(int page = 1)
        {
            page = page < 1
                ? 1
                : page;
            IQueryable<VisitorViewModel> query = await _visitorsService.GetActiveAsync<VisitorViewModel>();
            IPagedList<VisitorViewModel> pagedList =  query.ToPagedList(page, Constants.ItemsPerPage);
            return View(pagedList);
        }

        // GET: Visitors/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            if (id == Guid.Empty)
            {
                RedirectToAction("Index");
            }
            var dbEntity = await _visitorsService.GetAsync(id, x => x.Visits);
            if (dbEntity == null)
            {
                return NotFound();
            }

            var viewModel = Mapper.Map<VisitorDetailViewModel>(dbEntity);
            return View(viewModel);
        }

        // GET: Visitors/Create
        public IActionResult Create()
        {
            FillGenderList();
            return View();
        }

        // POST: Visitors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateVisitorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                FillGenderList();
                return View(model);
            }
            try
            {
                model.Id = Guid.NewGuid();
                var visitor = Mapper.Map<Visitor>(model);

                Visitor firstVisitor = await _visitorsService.GetByIinAsync(model.IIN);
                if (firstVisitor == null)
                {
                    await _visitorsService.AddAsync(visitor);
                    await _visitorsService.SaveAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    FillGenderList();
                    ModelState.AddModelError("Iin", "���������� � ����� ��� ��� ����������");
                    return View(model);
                }
            }
            catch (Exception e)
            {
                FillGenderList();
                ModelState.AddModelError("", e.Message);
                return View(model);
            }
        }

        // GET: Visitors/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            var dbEntity = await _visitorsService.GetAsync(id);
            if (dbEntity != null)
            {
                await _visitorsService.DeleteAsync(dbEntity);
                await _visitorsService.SaveAsync();
                return RedirectToAction("Index");
            }
            else
            {   //�������� ���������
                ViewData["Message"] = "��� ������";
                ViewBag.Message = "��� ������";
                return RedirectToAction("Index");
            }
        }

        private void FillGenderList()
        {
            Dictionary<string, string> selectDictionary = new Dictionary<string, string>();

            Type enumType = typeof(Gender);
            MemberInfo[] members = enumType.GetMembers();
            foreach (var memberInfo in members)
            {
                if (memberInfo.GetCustomAttribute(typeof(DisplayAttribute)) != null)
                {
                    selectDictionary.Add(memberInfo.Name,
                        ((DisplayAttribute)memberInfo.GetCustomAttribute(typeof(DisplayAttribute))).Name);
                }
            }

            ViewData.Add("GenderList", new SelectList(selectDictionary, "Key", "Value"));
        }
    }
}