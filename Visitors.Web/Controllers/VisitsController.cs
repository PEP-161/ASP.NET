using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Visitors.Domain;
using Visitors.Services;
using Visitors.Models.VisitsViewModels;
using X.PagedList;

namespace Visitors.Web.Controllers
{
    public class VisitsController : Controller
    {
        private readonly VisitorsService _visitorsService;
        private readonly PeopleService _peopleService;
        private readonly VisitsService _visitsService;


        public VisitsController(VisitorsService visitorService,
            PeopleService peopleService,
            VisitsService visitsService)
        {
            _visitorsService = visitorService;
            _peopleService = peopleService;
            _visitsService = visitsService;
        }

        // GET: Visitors
        public async Task<IActionResult> Index(int page = 1)
        {
            page = page < 1
                ? 1
                : page;
            var query = await _visitsService
                .GetActiveAsync<VisitViewModel>();
            var pagedList = query
                .ToPagedList(page, Constants.ItemsPerPage);
            return View(pagedList);
        }

        // GET: Visitors/Details/5
        public IActionResult Details(int id)
        {
            return View();
        }

        // GET: Visitors/Create
        public async Task<IActionResult> Create()
        {
            await FillDropDownList();
            return View();
        }

        // POST: Visitors/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateVisitViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newVisit = Mapper.Map<Visit>(model);
                await _visitsService.AddAsync(newVisit);
                await _visitorsService.SaveAsync();
                return RedirectToAction("Index");
            }
            await FillDropDownList();
            return View(model);
        }

        // GET: Visitors/Delete/5
        public IActionResult Delete(Guid id)
        {
            return View();
        }

        private async  Task FillDropDownList()
        {
            var visitors = new SelectList( await _visitorsService.GetDropDownRecordsAsync(), "Value", "ValueText");
            ViewData.Add("VisitorsList", visitors);

            var people = new SelectList( await _peopleService.GetDropDownRecordsAsync(), "Value", "ValueText");
            ViewData.Add("PeopleList", people);
        }
    }
}