﻿using System.Threading.Tasks;

namespace Visitors.Web.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
