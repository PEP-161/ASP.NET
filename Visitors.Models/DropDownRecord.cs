﻿namespace Visitors.Models
{
    public class DropDownRecord
    {
        public string Value { get; set; }
        public string ValueText { get; set; }
    }
}
