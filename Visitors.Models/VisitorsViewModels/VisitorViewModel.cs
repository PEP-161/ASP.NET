﻿using System;

namespace Visitors.Models.VisitorsViewModels
{
    public class VisitorViewModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public int TotalVisits { get; set; }
    }
}
