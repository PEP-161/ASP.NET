﻿using System;

namespace Visitors.Models.VisitsViewModels
{
    public class VisitViewModel
    {
        public DateTimeOffset VisitTime { get; set; }
        public int Floor { get; set; }
        public string Room { get; set; }
        public string VisitedPerson { get; set; }
        public string VisitorPerson { get; set; }
    }
}
